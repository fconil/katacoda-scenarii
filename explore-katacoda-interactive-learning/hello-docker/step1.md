This scenario has a Dockerfile which defines a Docker container to output an hello world message.

The command `docker build -t fcodvpt/docker-hello-world .`{{execute}} will create a Docker image is a friendly tag  _fcodvpt/docker-hello-world_ which we can use when starting a container based on the image.

The command `docker run fcodvpt/docker-hello-world`{{execute}} will run the container and display the message.
